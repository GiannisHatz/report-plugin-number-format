## org.universis.number-format

Use NumberFormatter class to format number as cardinal.

    NumberFormatter formatter = new NumberFormatter();
    
    String result = formatter.format(6.75, "en");
    // result = six and seventy-five hundredths
    
    result = formatter.format(9.75, "en", 3);
    // result = nine and seven hundred fifty thousandths
    
### NumberFormatter.format(number, locale)

Formats a number for the given locale
    
    NumberFormatter formatter = new NumberFormatter();
    String result = formatter.format(6.75, "en");
    // result = six and seventy-five hundredths

### NumberFormatter.format(number, locale, upperCase)

Formats a number for the given locale and returns an upper-case string.
    
    NumberFormatter formatter = new NumberFormatter();
    String result = formatter.format(6.75, "en", true);
    // result = SIX AND SEVENTY-FIVE HUNDREDTHS

### NumberFormatter.format(number, locale, fractionDigits)

Formats a number for the given locale and generates a cardinal with the specified fractional digits
    
    NumberFormatter formatter = new NumberFormatter();
    String result = formatter.format(9.75, "en", 3);
    // result = nine and seven hundred fifty thousandths
    
### NumberFormatter.format(number, locale, fractionDigits, upperCase)

Formats a number for the given locale and generates a cardinal with the specified fractional digits and returns an upper-case string.

    NumberFormatter formatter = new NumberFormatter();
    String res = formatter.format(14350, "en", true);
    // result = FOURTEEN THOUSAND THREE HUNDRED FIFTY
    
## Usage - JasperStudio

Use `NumberFormatter` class in jasperstudio by registering an external jar:

Right click Project -> Properties:

![JasperStudio- Project Properties](./jasper-studio-add-external-jar-click.png)

and select `org.universis.number-format.bundled.jar`

![JasperStudio- Add External Library](./jasper-studio-add-external-jar.png)

Use `NumberFormatter.format()` in report templates:

![JasperStudio- Report Design](./jasper-studio-use-number-format.png)

![JasperStudio - Preview Report](./jasper-studio-preview-report.png)

## Usage - JasperServer

Copy `org.universis.number-format.bundled.jar` to `WEB-INF/lib` directory.

## Build from sources

Clone git repository

    git clone https://gitlab.com/universis/report-plugin-number-format.git

Build library:

    mvn package
    